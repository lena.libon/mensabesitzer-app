package com.example.foodtogomensabesitzer.database;

public class Mensabesitzer {

    private String name, email, passwort;

    public Mensabesitzer(String name, String email, String passwort){
        this.name = name;
        this.email = email;
        this.passwort = passwort;
    }

    public Mensabesitzer(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getKey(){
        String child = email.replace(".", "dot");
        child.replace("@", "at");
        return child;
    }
}
