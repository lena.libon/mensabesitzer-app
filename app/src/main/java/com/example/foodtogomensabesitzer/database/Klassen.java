package com.example.foodtogomensabesitzer.database;

public class Klassen {
    private String klasse;

    public Klassen(String klasse){
        this.klasse = klasse;
    }

    public Klassen(){

    }

    public String getKlasse() {
        return klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }
}
