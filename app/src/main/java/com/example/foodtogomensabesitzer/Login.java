package com.example.foodtogomensabesitzer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogomensabesitzer.database.Mensabesitzer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class Login extends AppCompatActivity {

    private Button anmelden;
    private EditText email, passwort;
    private ImageButton passwortAuge;
    private CheckBox angemeldetBleiben;
    private int statusPasswort = 0; // 0: hide, 1: show

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        anmelden = findViewById(R.id.btnAnmelden);
        email = findViewById(R.id.emailadresseAnmelden);
        passwort = findViewById(R.id.passwortAnmelden);
        passwortAuge = findViewById(R.id.passwort_sichtbar_machen);
        angemeldetBleiben =  findViewById(R.id.checkbox_anmelden);

        Paper.init(this);

        String emailadresse = "";

        if (getIntent() != null){
            emailadresse = getIntent().getStringExtra("E-Mail");
        }

        if (emailadresse != null && !emailadresse.isEmpty()) {
            email.setText(emailadresse, TextView.BufferType.EDITABLE);
            Toast.makeText(Login.this, "Bitte gebe dein Passwort ein", Toast.LENGTH_SHORT).show();
        }

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mensabesitzer = database.getReference("Mensabesitzer");

        anmelden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (angemeldetBleiben.isChecked()){
                    String tempChild = email.getText().toString();
                    Paper.book().write("E-Mail", tempChild);
                    Paper.book().write("Passwort", passwort.getText().toString());
                }

                final ProgressDialog warten = new ProgressDialog(Login.this);
                warten.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                warten.setCancelable(false);
                warten.setMessage("Einen Moment Geduld bitte");
                warten.show();

                mensabesitzer.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String child = email.getText().toString().replace(".", "dot");
                        child.replace("@", "at");

                        // Auf Nullpointer durchsuchen
                        if (email.getText().toString().matches("")){
                            warten.dismiss();
                            Toast.makeText(Login.this, "Handynummer darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (passwort.getText().toString().matches("")){
                            warten.dismiss();
                            Toast.makeText(Login.this, "Passwort darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (snapshot.child(child).exists()){
                            // Benutzer schon in Datenbank eingetragen
                            warten.dismiss();
                            Mensabesitzer mensabesitzerNeu = snapshot.child(child).getValue(Mensabesitzer.class);
                            mensabesitzerNeu.setEmail(email.getText().toString());

                            // Passwort richtig
                            if (mensabesitzerNeu.getPasswort().equals(passwort.getText().toString())){
                                AktiverNutzer.aktivernutzer = mensabesitzerNeu;
                                AktiverNutzer.aktivernutzer.setEmail(email.getText().toString());
                                Intent startseite = new Intent(Login.this, Startseite.class);
                                startActivity(startseite);
                                Toast.makeText(Login.this, "Erfolgreich angemeldet!", Toast.LENGTH_SHORT).show();
                                finish();

                                mensabesitzer.removeEventListener(this);
                            }

                            // Passwort falsch
                            else{
                                Toast.makeText(Login.this, "Falsches Passwort!", Toast.LENGTH_SHORT).show();
                            }

                        }

                        // Benutzer noch nicht in die Datenbank eingetragen
                        else{
                            warten.dismiss();
                            Intent registrierenSeite = new Intent(Login.this, Registrieren.class);
                            startActivity(registrierenSeite);
                            Toast.makeText(Login.this, "Dieser Benutzer existiert nicht!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        warten.dismiss();
                        Intent fehler = new Intent(Login.this, FehlerDatenbankverbindung.class);
                        startActivity(fehler);
                        finish();
                    }
                });
            }
        });

        passwortAuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statusPasswort == 0){
                    passwort.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwort.setSelection(passwort.getText().length());

                    statusPasswort = 1;
                } else{
                    passwort.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwort.setSelection(passwort.getText().length());

                    statusPasswort = 0;
                }
            }
        });
}
}
