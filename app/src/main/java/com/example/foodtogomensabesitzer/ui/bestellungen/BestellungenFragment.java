package com.example.foodtogomensabesitzer.ui.bestellungen;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogomensabesitzer.ClickListener;
import com.example.foodtogomensabesitzer.R;
import com.example.foodtogomensabesitzer.UnterkategorieBestellung;
import com.example.foodtogomensabesitzer.database.Klassen;
import com.example.foodtogomensabesitzer.viewholders.KlassenViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class BestellungenFragment extends Fragment {

    private FirebaseDatabase database;
    private DatabaseReference klassen;
    private RecyclerView recyclerView;

    private FirebaseRecyclerAdapter<Klassen, KlassenViewHolder> adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bestellungen, container, false);

        // Init
        recyclerView = root.findViewById(R.id.recycler_view_klassenbestellung);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        database = FirebaseDatabase.getInstance();
        klassen = database.getReference("Klassen");

        FirebaseRecyclerOptions<Klassen> options = new FirebaseRecyclerOptions.Builder<Klassen>()
                .setQuery(klassen, Klassen.class).build();

        adapter = new FirebaseRecyclerAdapter<Klassen, KlassenViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull KlassenViewHolder holder, int position, @NonNull Klassen model) {
                holder.klasse.setText(model.getKlasse());
                final Klassen klasse = model;

                holder.setOnClickListener(new ClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean langeGeklickt) {
                        // Leite auf Bestellseite für einzelne Klassen um
                        Intent intent = new Intent(getActivity(), UnterkategorieBestellung.class);
                        intent.putExtra("Klasse", adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public KlassenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.klasse_spalte, parent, false);
                return new KlassenViewHolder(view);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);
        return root;
    }
}
