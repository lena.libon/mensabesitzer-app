package com.example.foodtogomensabesitzer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogomensabesitzer.database.Mensabesitzer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    private Button anmelden, registrieren;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        anmelden = findViewById(R.id.btnAnmeldenStart);
        registrieren = findViewById(R.id.btnRegistrierenStart);

        Paper.init(this);

        anmelden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        registrieren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Registrieren.class);
                startActivity(intent);
            }
        });

        anmelden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        });

        // Automatisches LOGIN
        final String emailadresse = Paper.book().read("E-Mail");
        final String passwort = Paper.book().read("Passwort");

        if (emailadresse != null && passwort != null && !emailadresse.isEmpty() && !passwort.isEmpty()){
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference mensabesitzerBenutzer = database.getReference("Mensabesitzer");

            // Anmelden
            final ProgressDialog warten = new ProgressDialog(MainActivity.this);
            warten.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            warten.setCancelable(false);
            warten.setMessage("Einen Moment Geduld bitte");
            warten.show();

            mensabesitzerBenutzer.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    // auf Nullpointer durchsuchen

                    String tempChild = emailadresse.replace(".", "dot");
                    tempChild.replace("@", "at");
                    if (snapshot.child(tempChild).exists()) {
                        warten.dismiss();

                        Mensabesitzer mensabesitzer = snapshot.child(tempChild).getValue(Mensabesitzer.class);
                        mensabesitzer.setEmail(emailadresse);
                        if (mensabesitzer.getPasswort().equals(passwort)){
                            Intent intent = new Intent(MainActivity.this, Startseite.class);
                            AktiverNutzer.aktivernutzer = mensabesitzer;
                            startActivity(intent);
                            Toast.makeText(MainActivity.this, "Erfolgreich angemeldet!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        // Passwort falsch
                        else {
                            Toast.makeText(MainActivity.this, "Falsches Passwort", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MainActivity.this, Login.class);
                            startActivity(intent);
                        }

                    }

                    // Benutzer noch nicht in die Datenbank eingetragen
                    else{
                        warten.dismiss();
                        Intent intent = new Intent(MainActivity.this, Registrieren.class);
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, "Diser Benutzer existiert nicht", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Intent intent = new Intent(MainActivity.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });
        }


    }
}
