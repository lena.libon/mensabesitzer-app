package com.example.foodtogomensabesitzer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogomensabesitzer.database.Produkt;
import com.example.foodtogomensabesitzer.viewholders.ProduktViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.UUID;

public class UnterkategorieEssen extends AppCompatActivity {

    private DatabaseReference produkt;
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private FirebaseRecyclerAdapter<Produkt, ProduktViewHolder> adapter;
    private FirebaseRecyclerOptions<Produkt> options;

    private EditText nameProdukt, preisProdukt, zusatzstoffeProdukt;
    private Button bildAuswaehlen;

    private Produkt neuesProdukt;
    private Uri speicherURI;
    private final int BILD_HOCHLADEN = 71;

    private boolean bildAusgewaehlt;

    private RecyclerView recyclerView;

    private String essenskategorieID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unterkategorie_essen);

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        produkt = database.getReference("Produkt");
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();


        FloatingActionButton fab = findViewById(R.id.fab_unterkategorie_essen);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuegeNeuesProduktHinzu();
            }
        });

        recyclerView = findViewById(R.id.recycler_view_essensauswahl);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        if (getIntent() != null)
            essenskategorieID = getIntent().getStringExtra("essenskategorieID");
        if (essenskategorieID != null && !essenskategorieID.isEmpty()){
            final Query query = produkt.orderByChild("essenskategorieID").equalTo(essenskategorieID);

            query.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot ds: dataSnapshot.getChildren()){
                        options = new FirebaseRecyclerOptions.Builder<Produkt>().setQuery(query,Produkt.class).build();
                        adapter = new FirebaseRecyclerAdapter<Produkt, ProduktViewHolder>(options) {
                            @Override
                            protected void onBindViewHolder(@NonNull ProduktViewHolder holder, int i, @NonNull Produkt produkt) {

                                holder.produnktname.setText(produkt.getName());
                                Picasso.get().load(produkt.getImage()).into(holder.bildProdukt);
                                final Produkt produktAusgewaehlt = produkt;
                                holder.setClickListener(new ClickListener() {
                                    @Override
                                    public void onClick(View view, int position, boolean isLongClick) {
                                        // Gehe auf die Seite des jeweiligen Produktes
                                        Intent intent = new Intent(UnterkategorieEssen.this, Produktseite.class);
                                        intent.putExtra("essenskategorieID", adapter.getRef(position).getKey());
                                        startActivity(intent);
                                    }
                                });
                            }

                            @NonNull
                            @Override
                            public ProduktViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.essensprodukt_spalte,parent,false);
                                return new ProduktViewHolder(view);
                            }
                        };
                        // Alternative mit Grid-Layout
                        // GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
                        // recyclerView.setLayoutManager(gridLayoutManager);
                        adapter.startListening();
                        recyclerView.setAdapter(adapter);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(UnterkategorieEssen.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    private void fuegeNeuesProduktHinzu() {
        bildAusgewaehlt = false;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(UnterkategorieEssen.this);
        alertDialog.setTitle("Neues Produkt hinzufügen");
        alertDialog.setIcon(R.drawable.ic_edit_black_24dp);
        alertDialog.setCancelable(true);

        LayoutInflater inflater = this.getLayoutInflater();
        final View neuesProduktView = inflater.inflate(R.layout.neues_produkt_erstellen, null);

        bildAuswaehlen = neuesProduktView.findViewById(R.id.btnProduktBildHochladen);
        nameProdukt = neuesProduktView.findViewById(R.id.name_neues_produkt);
        preisProdukt = neuesProduktView.findViewById(R.id.preis_neues_produkt);
        zusatzstoffeProdukt = neuesProduktView.findViewById(R.id.zusatzstoffe_neues_produkt);

        bildAuswaehlen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waehleBildAus();
            }
        });

        alertDialog.setView(neuesProduktView);

        alertDialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (nameProdukt.getText().toString().isEmpty()){
                    // TODO dialog soll nicht schließen
                } else if (!bildAusgewaehlt){
                    // TODO dialog soll nicht schließen
                } else if (preisProdukt.getText().toString().isEmpty()){
                    // TODO dialog soll nicht schließen
                } else if (zusatzstoffeProdukt.getText().toString().isEmpty()){
                    // TODO dialog soll nicht schließen
                }else {
                    ladeBildHoch();
                    dialog.dismiss();
                }
            }
        });

        alertDialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void ladeBildHoch() {
        if (speicherURI != null){
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Bild wird hochgeladen");
            dialog.show();

            String name = UUID.randomUUID().toString();
            final StorageReference imgFolder = storageReference.child("Essen-Unterbegriffe/"+name);
            imgFolder.putFile(speicherURI).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(UnterkategorieEssen.this, "Bild erfolgreich hochgeladen", Toast.LENGTH_SHORT).show();
                    imgFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            neuesProdukt = new Produkt(essenskategorieID, uri.toString(), nameProdukt.getText().toString(),
                                    preisProdukt.getText().toString(), zusatzstoffeProdukt.getText().toString());
                            produkt.child(neuesProdukt.getName()).setValue(neuesProdukt);
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(UnterkategorieEssen.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.setMessage("Bild wird hochgeladen...");
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BILD_HOCHLADEN && resultCode == RESULT_OK && data != null && data.getData() != null){
            speicherURI = data.getData();
            bildAuswaehlen.setText("Bild ausgewählt");
            bildAuswaehlen.setBackgroundColor(Color.parseColor("#218041"));
            bildAuswaehlen.setTextColor(Color.WHITE);
            bildAusgewaehlt = true;
        }

    }

    private void waehleBildAus() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Passendes Bild auswählen"), BILD_HOCHLADEN);

    }
}

