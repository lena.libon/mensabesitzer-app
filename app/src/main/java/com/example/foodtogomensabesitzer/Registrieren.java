package com.example.foodtogomensabesitzer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogomensabesitzer.database.Mensabesitzer;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Registrieren extends AppCompatActivity {

    private EditText name, email, passwort;
    private Button registrieren;
    private int statusPasswort = 0; // 0: hide, 1: show
    private ImageButton passwortAuge;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrieren);

        name = findViewById(R.id.nameRegistrieren);
        email = findViewById(R.id.emailRegistrieren);
        passwort = findViewById(R.id.passwortRegistrieren);

        registrieren = findViewById(R.id.btnRegistrieren);
        passwortAuge = findViewById(R.id.passwort_sichtbar_machen_registrieren);

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mensabesitzer = database.getReference("Mensabesitzer");

        registrieren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog warten = new ProgressDialog(Registrieren.this);
                warten.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                warten.setCancelable(false);
                warten.setMessage("Einen Moment Geduld bitte");
                warten.show();

                mensabesitzer.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String child = email.getText().toString().replace(".", "dot");
                        child.replace("@", "at");

                        if (name.getText().toString().matches("")) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "Name darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (name.getText().toString().split(" ").length != 2) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "Name soll im Format 'Vorname Nachname' sein", Toast.LENGTH_SHORT).show();
                        } else if (email.getText().toString().matches("")) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "E-Mail-Adresse darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (email.getText().toString().split("@").length != 2) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte geben Sie eine gültige E-Mail-Adresse an!", Toast.LENGTH_SHORT).show();
                        } else if (passwort.getText().toString().matches("")) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "Passwort darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (passwort.getText().toString().length() < 6) {
                            warten.dismiss();
                            Toast.makeText(Registrieren.this, "Passwort muss mindestens die Länge 6 haben", Toast.LENGTH_SHORT).show();
                        } else if (snapshot.child(child).exists()) {
                            // Benutzer schon registriert
                            warten.dismiss();
                            Intent anmeldeSeite = new Intent(Registrieren.this, Login.class);
                            anmeldeSeite.putExtra("E-Mail", email.getText().toString());
                            startActivity(anmeldeSeite);
                        }

                        // Registriere neuen Benutzer
                        else {
                            warten.dismiss();
                            Mensabesitzer mensabesitzerNeu = new Mensabesitzer(name.getText().toString(), email.getText().toString(), passwort.getText().toString());

                            mensabesitzer.child(child).setValue(mensabesitzerNeu);
                            AktiverNutzer.aktivernutzer = mensabesitzerNeu;

                            Intent intent = new Intent(Registrieren.this, Startseite.class);
                            startActivity(intent);

                            Toast.makeText(Registrieren.this, "Erfolgreich registriert", Toast.LENGTH_SHORT).show();

                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        warten.dismiss();
                        Intent fehlerDatenbankverbindung = new Intent(Registrieren.this, FehlerDatenbankverbindung.class);
                        startActivity(fehlerDatenbankverbindung);
                        finish();
                    }
                });

            }

            });

        passwortAuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statusPasswort == 0){
                    passwort.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwort.setSelection(passwort.getText().length());
                    statusPasswort = 1;
                } else{
                    passwort.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwort.setSelection(passwort.getText().length());
                    statusPasswort = 0;
                }
            }
        });

    }
}
