package com.example.foodtogomensabesitzer;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogomensabesitzer.database.Bestellung;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;
import java.util.List;

public class UnterkategorieBestellung extends AppCompatActivity {

    private TextView ueberschriftKlasse;
    private RecyclerView recyclerView;
    private RecyclerViewAdapterBestellung adapterBestellung;

    FirebaseDatabase database;
    DatabaseReference bestellung;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unterkategorie_bestellung);

        // INIT
        ueberschriftKlasse = findViewById(R.id.txt_bestellungskategorie);
        recyclerView = findViewById(R.id.recycler_view_bestellungen);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Firebase
        database = FirebaseDatabase.getInstance();
        bestellung = database.getReference("Bestellung");

        // Setze Überschrift, lade passende Bestellungen
        String klasse = "";
        if (getIntent() != null)
            klasse = getIntent().getStringExtra("Klasse");
        if (klasse != null && !klasse.isEmpty()) {
            ueberschriftKlasse.setText("Klasse " + klasse);

            final List<Bestellung> listBestellung = new LinkedList<>();
            final List<String> listIDs = new LinkedList<>();

            final Query query = bestellung.orderByChild("klasse").equalTo(klasse);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                        Bestellung bestellungTemp = snapshot1.getValue(Bestellung.class);
                        listBestellung.add(bestellungTemp);
                        listIDs.add(snapshot1.getKey());

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Intent intent = new Intent(UnterkategorieBestellung.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });

            adapterBestellung = new RecyclerViewAdapterBestellung(this, listBestellung, listIDs);
            recyclerView.setAdapter(adapterBestellung);
        }
    }
}
