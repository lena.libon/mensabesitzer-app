package com.example.foodtogomensabesitzer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.foodtogomensabesitzer.database.Essenskategorien;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.paperdb.Paper;

public class Startseite extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private TextView name_admin, email_admin;
    private NavController navController;

    private FirebaseDatabase database;
    private StorageReference storageReference;
    private FirebaseStorage firebaseStorage;
    private DatabaseReference essenskategorien;

    private int status1 = 0, status2 = 0, status3 = 0; //0: hide, 1: show
    private AppBarConfiguration appBarConfiguration;

    private EditText nameEssenskategorie;
    private Button bildAuswaehler;

    private Essenskategorien newEssenskategorie;
    private Uri speicherURI;
    private final int BILD_HOCHLADEN = 71;

    private boolean bildAusgewaehlt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startseite);
        Toolbar toolbar = findViewById(R.id.toolbar);

        Paper.init(this);

        database = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
        essenskategorien = database.getReference("Essenskategorien");

        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuegeNeueKategorieHinzu();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.getMenu().findItem(R.id.nav_logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                logout();
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.nav_passwort).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                passwortAendern();
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.nav_email).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                emailAendern();
                return true;
            }
        });

        MenuItem profilUeberbegriff = navigationView.getMenu().findItem(R.id.profilUeberbegriff);
        SpannableString spannableString = new SpannableString(profilUeberbegriff.getTitle());
        spannableString.setSpan(new TextAppearanceSpan(this, R.style.Profil), 0, spannableString.length(), 0);
        profilUeberbegriff.setTitle(spannableString);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_startseite, R.id.nav_bestellungen, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        // Informationen des Nutzers setzen
        View headerView = navigationView.getHeaderView(0);
        name_admin = headerView.findViewById(R.id.name_admin);
        email_admin = headerView.findViewById(R.id.email_admin);
        name_admin.setText(AktiverNutzer.aktivernutzer.getName());
        email_admin.setText(AktiverNutzer.aktivernutzer.getEmail());

    }

    private void fuegeNeueKategorieHinzu() {
        bildAusgewaehlt = false;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Startseite.this);
        alertDialog.setTitle("Neue Essenskategorie hinzufügen");
        alertDialog.setIcon(R.drawable.ic_edit_black_24dp);
        alertDialog.setCancelable(true);

        LayoutInflater inflater = this.getLayoutInflater();
        final View neueEssenskategorie = inflater.inflate(R.layout.neue_essenskategorie_erstellen, null);

        bildAuswaehler = neueEssenskategorie.findViewById(R.id.btnEssenskategorieBildHochladen);
        nameEssenskategorie = neueEssenskategorie.findViewById(R.id.name_neue_essenskategorie);

        bildAuswaehler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waehleBildAus();
            }
        });

        alertDialog.setView(neueEssenskategorie);

        alertDialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (nameEssenskategorie.getText().toString().isEmpty()){
                    // TODO dialog soll nicht schließen
                } else if (!bildAusgewaehlt){
                    // TODO dialog soll nicht schließen
                }
                else {
                    ladeBildHoch();
                    dialog.dismiss();
                }
            }
        });

        alertDialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void ladeBildHoch() {
        if (speicherURI != null){
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Bild wird hochgeladen");
            dialog.show();

            String name = UUID.randomUUID().toString();
            final StorageReference imgFolder = storageReference.child("Essen-Überbegriffe/"+name);
            imgFolder.putFile(speicherURI).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    Toast.makeText(Startseite.this, "Bild erfolgreich hochgeladen", Toast.LENGTH_SHORT).show();
                    imgFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            newEssenskategorie = new Essenskategorien(uri.toString(), nameEssenskategorie.getText().toString());
                            essenskategorien.child(newEssenskategorie.getName()).setValue(newEssenskategorie);
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(Startseite.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.setMessage("Bild wird hochgeladen...");
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BILD_HOCHLADEN && resultCode == RESULT_OK && data != null && data.getData() != null){
            speicherURI = data.getData();
            bildAuswaehler.setText("Bild ausgewählt");
            bildAuswaehler.setBackgroundColor(Color.parseColor("#218041"));
            bildAuswaehler.setTextColor(Color.WHITE);
            bildAusgewaehlt = true;
        }

    }

    private void waehleBildAus() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Passendes Bild auswählen"), BILD_HOCHLADEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.startseite, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void logout(){
        // Lösche den gespeicherten Benutzer
        Paper.book().destroy();

        Intent intent = new Intent(Startseite.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void passwortAendern(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Startseite.this);
        alertDialog.setTitle("Passwort ändern");
        alertDialog.setIcon(R.drawable.ic_edit_black_24dp);
        alertDialog.setCancelable(true);

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.passwort_aendern, null);

        alertDialog.setView(layout);

        final EditText passwortBisher = layout.findViewById(R.id.passwortBisher);
        final EditText passwortNeu1 = layout.findViewById(R.id.passwortNeu1);
        final EditText passwortNeu2 = layout.findViewById(R.id.passwortNeu2);

        ImageButton augePasswortBisher = layout.findViewById(R.id.passwort_sichtbar_machen_1);
        ImageButton augePasswortNeu1 = layout.findViewById(R.id.passwort_sichtbar_machen_2);
        ImageButton augePasswortNeu2 = layout.findViewById(R.id.passwort_sichtbar_machen_3);

        status1 = 0;
        status2 = 0;
        status3 = 0;

        augePasswortBisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status1 == 0){
                    passwortBisher.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwortBisher.setSelection(passwortBisher.getText().length());

                    status1 = 1;
                } else{
                    passwortBisher.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwortBisher.setSelection(passwortBisher.getText().length());

                    status1 = 0;
                }
            }
        });

        augePasswortNeu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status2 == 0){
                    passwortNeu1.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwortNeu1.setSelection(passwortNeu1.getText().length());

                    status2 = 1;
                } else{
                    passwortNeu1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwortNeu1.setSelection(passwortNeu1.getText().length());

                    status2 = 0;
                }
            }
        });

        augePasswortNeu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status3 == 0){
                    passwortNeu2.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwortNeu2.setSelection(passwortNeu2.getText().length());

                    status3 = 1;
                } else{
                    passwortNeu2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwortNeu2.setSelection(passwortNeu2.getText().length());

                    status3 = 0;
                }
            }
        });

        alertDialog.setPositiveButton("Ändern", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Neues Passwort setzen
                if (passwortBisher.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte gebe dein bisheriges Passwort ein", Toast.LENGTH_SHORT).show();
                else if (passwortNeu1.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte gebe ein neues Passwort ein", Toast.LENGTH_SHORT).show();
                else if (passwortNeu2.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte wiederhole dein neues Passwort", Toast.LENGTH_SHORT).show();
                    // Früheres Passwort falsch
                else if (!passwortBisher.getText().toString().equals(AktiverNutzer.aktivernutzer.getPasswort()))
                    Toast.makeText(Startseite.this, "Bisheriges Passwort ist falsch", Toast.LENGTH_SHORT).show();
                    // Früheres Passwort richtig
                else{
                    if (!passwortNeu1.getText().toString().equals(passwortNeu2.getText().toString()))
                        Toast.makeText(Startseite.this, "Wiederholtes Passwort stimmt nicht mit dem Neuen überein", Toast.LENGTH_SHORT).show();
                    else {
                        if (passwortNeu1.getText().toString().equals(passwortBisher.getText().toString()))
                            Toast.makeText(Startseite.this, "Bitte gebe ein neues Passwort ein", Toast.LENGTH_SHORT).show();
                        else if (passwortNeu1.getText().toString().length() < 6)
                            Toast.makeText(Startseite.this, "Passwort muss mindestens die Länge 6 haben", Toast.LENGTH_SHORT).show();
                        else{
                            Map<String, Object> neuesPasswort = new HashMap<>();
                            neuesPasswort.put("passwort", passwortNeu1.getText().toString());

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference benutzer = database.getReference("Benutzer");

                            benutzer.child(AktiverNutzer.aktivernutzer.getKey())
                                    .updateChildren(neuesPasswort)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            AktiverNutzer.aktivernutzer.setPasswort(passwortNeu1.getText().toString());
                                            Toast.makeText(Startseite.this, "Passwort erfolgreich geändert", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(Startseite.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                }
            }
        });

        alertDialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void emailAendern(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Startseite.this);
        alertDialog.setTitle("Email ändern");
        alertDialog.setIcon(R.drawable.ic_edit_black_24dp);

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.email_aendern, null);

        alertDialog.setView(layout);

        final EditText emailBisher = layout.findViewById(R.id.emailBisher);
        final EditText emailNeu1 = layout.findViewById(R.id.emailNeu1);
        final EditText emailNeu2 = layout.findViewById(R.id.emailNeu2);

        alertDialog.setPositiveButton("Ändern", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Neues Passwort setzen
                if (emailBisher.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte gebe deine bisherige E-Mail-Adresse ein", Toast.LENGTH_SHORT).show();
                else if (emailNeu1.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte gebe eine neue E-Mail-Adresse ein", Toast.LENGTH_SHORT).show();
                else if (emailNeu2.getText().toString().isEmpty())
                    Toast.makeText(Startseite.this, "Bitte wiederhole deine neue E-Mail-Adresse", Toast.LENGTH_SHORT).show();
                    // Frühere Email falsch
                else if (!emailBisher.getText().toString().equals(AktiverNutzer.aktivernutzer.getEmail()))
                    Toast.makeText(Startseite.this, "Bisherige E-Mail-Adresse ist falsch", Toast.LENGTH_SHORT).show();
                    // Früheres Passwort richtig
                else{
                    if (!emailNeu1.getText().toString().equals(emailNeu2.getText().toString()))
                        Toast.makeText(Startseite.this, "Wiederholte E-Mail-Adresse stimmt nicht mit der Neuen überein", Toast.LENGTH_SHORT).show();
                    else {
                        if (emailNeu1.getText().toString().equals(emailBisher.getText().toString()))
                            Toast.makeText(Startseite.this, "Bitte gebe eine neue E-Mail-Adresse ein", Toast.LENGTH_SHORT).show();
                        else if (emailNeu1.getText().toString().split("@").length!=2)
                            Toast.makeText(Startseite.this, "Bitte geben Sie eine gültige E-Mail-Adresse an!", Toast.LENGTH_SHORT).show();
                        else{
                            Map<String, Object> neueEmail = new HashMap<>();
                            neueEmail.put("email", emailNeu1.getText().toString());

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference benutzer = database.getReference("Benutzer");

                            benutzer.child(AktiverNutzer.aktivernutzer.getKey())
                                    .updateChildren(neueEmail)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            AktiverNutzer.aktivernutzer.setPasswort(emailNeu1.getText().toString());
                                            Toast.makeText(Startseite.this, "E-Mail-Adresse erfolgreich geändert", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(Startseite.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                }
            }
        });

        alertDialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
