package com.example.foodtogomensabesitzer.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogomensabesitzer.ClickListener;
import com.example.foodtogomensabesitzer.R;

public class KlassenViewHolder  extends RecyclerView.ViewHolder implements  View.OnClickListener {
    public TextView klasse;
    private ClickListener clickListener;

    public KlassenViewHolder(@NonNull View itemView){
        super(itemView);

        klasse = itemView.findViewById(R.id.klasse_recyclerview);
        itemView.setOnClickListener(this);
    }

    public void setOnClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        clickListener.onClick(v, getAdapterPosition(), false);
    }
}
