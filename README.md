# MGMensa - App zur Essensbestellung beim Pausenverkauf/in der Mensa 
### ~ App für den Mensabesitzer ~
Admin-App für die Mensa App des MGMs (https://gitlab.com/lena.libon/mensa-app). 

Entstanden durch das Softwareprojekt in der Q11. 


![1](/uploads/46763b4940dff34378af083f9baeed2d/1.png)

**Funktionen:**

- Essenskategorien/Produkte anschauen 
- Neue Essenskategorien/Produkte erstellen 
- Bestellungen nach Klasse geordnet anschauen 
- Passwort/Email ändern 
- Automatisches Login

**Gallerie**

![Screenshot_1601843321](/uploads/197c297c2538b497f06a7d78d5ec9d22/Screenshot_1601843321.png)

![Screenshot_1601843368](/uploads/4225f60ca94ea86951955ac09e0c07b6/Screenshot_1601843368.png)

![Screenshot_1601843393](/uploads/96a34d854f6ffcc6aaa90af9931666cb/Screenshot_1601843393.png)